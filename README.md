# kpdux-orm

### Install
```
npm install kpdux kpdux-orm redux redux-orm
```


### Example
```JavaScript
import kpdux from "kpdux";
import {createOrmModule} from "kpdux-orm";

const store = kpdux.createStore({
    "orm": createOrmModule({
        "ModelA": {
            "id": {},
            "title": {}
        }
    })
});

```
