import Model from "./Model";


class ORM {
    constructor() {
        this.models = {};
        this.state = {};
    }

    register(name, fields) {
        let model = new Model(this, name, fields);

        this.models.push(model);
    }

    setState(state) {
        this.state = state;
    }

    getState() {

    }

    getModel(name) {

    }

    get(type, id) {

    }

    find() {

    }
}


export default ORM;