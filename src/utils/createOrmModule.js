import {
    ORM,
    Model as ModelORM,
    attr
} from "redux-orm";

import actions from "./../actions";


export default (models = {}, module = {}) => {
    const orm = new ORM();

    for(let name in models) {
        let fields = {};

        if(typeof models[name] === "function") {
            orm.register(models[name]);
        }
        else {
            for(let field in models[name]) {
                let options = models[name][field];

                // console.log(name, field, options);

                fields[field] = attr();
            }

            let Model = class Model extends ModelORM {};

            Model.modelName = name;
            Model.fields = fields;

            orm.register(Model);
        }
    }

    return {
        init(store) {
            store.kpduxOrm = this.name;
        },
        "state": {
            ...(module.state || {}),
            ...orm.getEmptyState()
        },
        "reduces": {
            ...(module.reducers || {}),
            [actions.create]: function(state, action) {
                const session = orm.session(state);

                const Model = session.sessionBoundModels.find((model) => {
                    return model.modelName === action.name;
                });

                if(Model) {
                    if(Model.idExists(action.item.id)) {
                        Model.withId(action.id).update(action.item);
                    }
                    else {
                        Model.create(action.item);
                    }
                }

                return session.state;
            },
            [actions.update]: function(state, action) {
                const session = orm.session(state);

                const Model = session.sessionBoundModels.find((model) => {
                    return model.modelName === action.name;
                });

                if(Model) {
                    Model.withId(action.id).update(action.item);
                }

                return session.state;
            },
            [actions.remove]: function(state, action) {
                const session = orm.session(state);

                const Model = session.sessionBoundModels.find((model) => {
                    return model.modelName === action.name;
                });

                if(Model) {
                    Model.withId(action.id).delete();
                }

                return session.state;
            }
        },
        "middlewares": {
            ...(module.middlewares || {})
        },
        "getters": {
            ...(module.getters || {}),
            getSession(state) {
                return orm.session(state);
            },
            getModel(state, getters) {
                const session = this.getSession();

                return (type) => {
                    return session.sessionBoundModels.find((model) => {
                        return model.modelName === type;
                    });
                };
            },
            getItem(state) {
                return (type, id) => {
                    const Model = this.getModel()(type);

                    if(Model) {
                        let item = Model.withId(id);

                        return item && item.ref;
                    }

                    return null;
                };
            },
            getItems(state) {
                return (type) => {
                    const Model = this.getModel()(type);

                    if(Model) {
                        return Model.all().toRefArray();
                    }
                    else {
                        console.error("Model '" + type + "' is missing.");

                        return [];
                    }
                };
            },
            search(state) {
                return (type, params) => {
                    const Model = this.getModel()(type);

                    if(Model) {
                        return Model._findDatabaseRows(params);
                    }

                    return [];
                };
            },
            getIds(state) {
                return (type) => {
                    if(state[type]) {
                        return state[type].items;
                    }

                    return [];
                }
            },
            getMaxId(state) {
                return (type) => {
                    return state[type].meta.maxId;
                };
            }
        },
        "mutations": {
            ...(module.mutations || {})
        },
        "actions": {
            ...(module.actions || {}),
            addItem(type, item) {
                return this.store.dispatch({
                    "type": actions.create,
                    "name": type,
                    "item": item
                });
            },
            updateItem(type, id, item) {
                return this.store.dispatch({
                    "type": actions.update,
                    "name": type,
                    "id": id,
                    "item": item
                });
            },
            removeItem(type, id) {
                return this.store.dispatch({
                    "type": actions.remove,
                    "name": type,
                    "id": id
                });
            }
        }
    };
};