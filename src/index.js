import {
    Model,
    attr
} from "redux-orm";
import createOrmModule from "./utils/createOrmModule";
import createEntityBeta from "./utils/createEntityBeta";
import createOrm from "./utils/createOrm";


export {
    Model,
    attr,
    createOrmModule,
    createEntityBeta,
    createOrm
};

export default {
    "Model": Model,
    "attr": attr,
    "createOrmModule": createOrmModule,
    "createEntityBeta": createEntityBeta,
    "createOrm": createOrm
};